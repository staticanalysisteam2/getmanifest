import re
import os



'''
Gets manifest as input and writes a txt file containing all the permissions
the apk file has, each in a different line.
'''
def getPermission(path, target="permissions.txt"):
    if isinstance(path, str) and os.path.isfile(path):
        res = []
        code = re.compile("<uses-permission[^\"]*\"([^\"]*)\"")
        file = open(path, "r")
        text = file.read()
        itr = re.findall(code, text)
        file = open(target, "w")
        for line in itr:
            file.write(line + "\n")
        file.close()
    else:
        print("ERROR OPENING FILE")



'''
Gets manifest as input and outputs a vector represesnting the original Android
application. Features:
    - Permissions
    - Size of the original apk file
    - Number of new permissions created by apk file author
'''
def vectorize(path, known_p="permissionList.txt"):
    #TODO: PUT SIZE IN VECTOR: res[-2] = getSizeOfAPK
        if isinstance(path, str) and os.path.isfile(path):
            #writes in a file (permissions.txt) what permissions the manifest has
            getPermission(path)
            known_perms_file = open(known_p, "r")
            known_perms = known_perms_file.readlines()
            known_perms_file.close()
            res = [0 for i in range(len(known_perms)+2)]
            file = open("permissions.txt", "r")
            perms = file.readlines()
            #get rid of "\n"'s
            for i in range(len(perms)):
                perms[i] = perms[i].replace("\n", "")
            perms = set(perms)
            #go on all known permissions. if one of them appears in one of the
            #manifest's permissions, mark it (res[i] = 1), and remove from manifest
            #permissions (perms). the permissions left in perms are unknown ones,
            #which the number of them will be listed later in res[-1]
            for i in range(len(known_perms)):
                raw_permission = known_perms[i].replace("\n", "")
                if raw_permission in perms:
                    res[i] = 1
                    perms.remove(raw_permission)
            res[-1] = len(perms)

           #WHAT ARE THOSE TWO LINES:
            
           # res.appned(os.path.size(path))
           # res.append(extra files size)
            return res
        else:
            print("ERROR OPENING FILE")
